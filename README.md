# Abu - Endpoint Informator

Webstore: https://chrome.google.com/webstore/detail/abu-endpoint-informator/fiplgaonillajmciijmaaomebgbdjmoj

Displays basic information about the currently viewed endpoint. Default shortcut key: Ctrl+Shift+1
When the extension is configured properly and the endpoint specific branch is set,
the following informations will be available for review and ticket creation:
- Version number of the browser
- Viewport size
- Base URL of the currently viewed endpoint
- Environment name of the endpoint
- Deployed product version number on the endpoint
- Application name
- Deployed build number
- Deployment state
- State of the latest finished build
- Indication of an active build process

**Optional resources (JSON):**
- Product name: endpoint.net/metaData.json (Default inspected property: name)
- UI version: endpoint.net/metaData.json (Default inspected property: version)
- Host: endpoint.net/host.js (Default inspected property: environment || envName)
- Current build: ci.net/rest/api/latest/plan/{branch} (Default inspected tag: isBuilding : true // false)
- Finished build: ci.net/rest/api/latest/result/{branch}/latest (Default inspected tag: buildState : Successful // Failed)
- CD deliveries: cd.net/cd/api/delivery/list
- CD endpoint search: cd.net/cd/api/search/{delivery}/?type={deliveryType}&{query}
- Auto-Complete Version: aux.net/api/AutoCompleteSvc
- Platform Version: aux.net/api/platform
- Search Service Version: aux.net/api/srch
- Email Template Version : aux.net/api/emailSvc/TEMPLATES

### Installing Dev. requirements:
- Prerequisites: [NodeJS 16.x+ version](https://nodejs.org/en/), [Git](https://git-scm.com/)

### Installation of the Dev. Environment:
Write the followings into a terminal standing in the project folder:
    
    $> yarn [install]
        
### Running tests
In the root folder run a command like:

    $> yarn test:unit
    $> yarn test:e2e

### Installation of the unpacked extension:
- Open chrome://extensions/
- Turn on the Developer mode
- Load unpacked folder: extension
- Import the varstemp file from the product_configs folder
