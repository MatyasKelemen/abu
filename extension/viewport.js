"use strict";

(function saveViewportSize() {
    const vw = Math.max(document.documentElement.clientWidth || 0, window.innerWidth || 0);
    const vh = Math.max(document.documentElement.clientHeight || 0, window.innerHeight || 0);

    chrome.storage.local.set({
        sWidth: vw,
        sHeight: vh,
    }, () => {
        console.log("Viewport size: saved");
    });
}());
