"use strict";

// Wrap div elements around the global variable names and values
function generateHtml(arr) {
    if (arr) {
        return Object.keys(arr).reduce(
            (accum, currKey) => `${accum}<div class="variableName">${currKey}</div>
            <div class="variableValue">${arr[currKey]}</div><br>`,
            "",
        );
    }
}

// Print out the saved variables from the chrome.storage
function printGlobals() {
    chrome.storage.local.get(null, items => {
        if (!chrome.runtime.lastError) {
            let generatedHtml = "";
            if (Object.keys(items).length > 0) {
                if (items.globals) {
                    generatedHtml += generateHtml(items.globals);
                    delete items.globals;
                }
                generatedHtml += generateHtml(items);
            }
            document.getElementById("urlOverview").innerHTML = generatedHtml;
        }
    });
}

function getEditMode() {
    const editDescOnly = document.getElementById("editDescOnly").value;
    if (editDescOnly === "On") {
        document.getElementById("editDescOnly").value = "Off";
        return {
            editDescOnly: "Off",
        };
    }
    document.getElementById("editDescOnly").value = "On";
    return {
        editDescOnly: "On",
    };
}

// Save options to chrome.storage
function saveOptions() {
    const options = getEditMode();
    chrome.storage.local.set(options, () => {
        if (!chrome.runtime.lastError) {
            const status = document.getElementById("settingsStatus");
            status.textContent = "Options saved.";
            setTimeout(() => {
                status.textContent = "";
            }, 2500);
            printGlobals();
        }
    });
}

// Restore button state using the preferences stored in chrome.storage
function restoreOptions() {
    // Default value editDescOnly = "On"
    chrome.storage.local.get({
        editDescOnly: "On",
    }, items => {
        if (!chrome.runtime.lastError) {
            document.getElementById("editDescOnly").value = items.editDescOnly;
        }
    });
}

// Remove all branch name from the chrome.storage
function clearStorage() {
    const text = "The Chrome Storage will be cleared!\nPlease confirm.";
    // eslint-disable-next-line no-alert
    if (confirm(text) === true) {
        chrome.storage.local.clear(() => {
            if (!chrome.runtime.lastError) {
                document.getElementById("fileInput").value = "";
                const status = document.getElementById("settingsStatus");
                status.textContent = "Cleared. Upload a vars.json file!";
                setTimeout(() => {
                    status.textContent = "";
                }, 2500);
                console.log("The Chrome storage is cleared.");
                printGlobals();
                restoreOptions();
            }
        });
    }
}

// Remove all the saved branches from the chrome.storage
function clearBranches() {
    chrome.storage.local.get(null, items => {
        const filteredBranches = Object.fromEntries(Object.entries(items).filter(([key]) => /^usr/.test(key)));
        chrome.storage.local.remove(Object.keys(filteredBranches), () => {
            if (!chrome.runtime.lastError) {
                const status = document.getElementById("settingsStatus");
                status.textContent = "The branches are cleared.";
                setTimeout(() => {
                    status.textContent = "";
                }, 2500);
                console.log("The branches are cleared.");
                printGlobals();
            }
        });
    });
}

function openUserManual() {
    chrome.tabs.create({
        url: "man.html",
    });
}

function setKeyboardShortcuts() {
    chrome.tabs.create({
        url: "chrome://extensions/configureCommands",
    });
}

function importJSON() {
    const file = document.getElementById("fileInput").files[0];
    const status = document.getElementById("settingsStatus");
    const reader = new FileReader();
    reader.onload = () => {
        try {
            const parsedFile = JSON.parse(reader.result);
            chrome.storage.local.set(parsedFile, () => {
                if (chrome.runtime.lastError) {
                    status.textContent = "Error: Check the selected vars.json file!";
                    setTimeout(() => {
                        status.textContent = "";
                    }, 2500);
                    console.error("Error during saving file vars.json");
                } else {
                    status.textContent = "Successful setup.";
                    setTimeout(() => {
                        document.getElementById("fileInput").value = "";
                        status.textContent = "";
                    }, 2500);
                    console.log("The vars.json file is saved in Chrome Storage.");
                    printGlobals();
                }
            });
        } catch (error) {
            status.textContent = "Error: Invalid JSON file!";
            setTimeout(() => {
                status.textContent = "";
            }, 2500);
            console.log("Error during parsing the selected vars.json file!");
        }
    };
    reader.readAsText(file);
}

// Set shortcut key
document.getElementById("hotkey").addEventListener("click", setKeyboardShortcuts);

// Remove saved branches
document.getElementById("clear").addEventListener("click", clearStorage);

// Clear the dropdown
document.getElementById("clearBranches").addEventListener("click", clearBranches);

// User manual
document.getElementById("userManual").addEventListener("click", openUserManual);

// Setting up the global variables
document.getElementById("fileInput").addEventListener("change", importJSON);

// Enable/Disable ticket summary editing
document.getElementById("editDescOnly").addEventListener("click", saveOptions);

// Print global variables
document.addEventListener("DOMContentLoaded", () => {
    printGlobals();
    restoreOptions();
});
