"use strict";

const branchForm = document.getElementById("branch");
const ticketForm = document.getElementById("ticket");
const gl = {};
const rt = {};

function displayOnThePopup(element, content, checkboxId) {
    try {
        element = document.getElementById(element);
        switch (element.type) {
            case "button":
                element.removeAttribute("disabled");
                break;
            default:
                element.innerHTML = content ? content : "";
                break;
        }
        if (checkboxId) {
            checkboxId = document.getElementById(checkboxId);
            checkboxId.style.display = "inline";
        }
    } catch (error) {
        console.error(error);
    }
}

function hideFromThePopup(element, checkboxId) {
    try {
        element = document.getElementById(element);
        switch (element.type) {
            case "text":
                element.value = "";
                break;
            case "button":
                element.setAttribute("disabled", "disabled");
                break;
            default:
                element.innerHTML = "";
                break;
        }
        if (checkboxId) {
            checkboxId = document.getElementById(checkboxId);
            checkboxId.style.display = "none";
        }
    } catch (error) {
        console.error(error);
    }
}

function getMatchingPropertyName(obj, pattern) {
    return Object.keys(obj).filter(key => pattern.test(key));
}

// Gives back a value from an object using a path which is stored in an array
function getValue(obj, path, placeHoldersObj) {
    try {
        obj = Array.isArray(obj) ? obj[0] : obj;
        let pathArray = Array.isArray(path) ? path : [path];
        if (Object.keys(obj).length === 0 && pathArray.length === 0) {
            throw new Error("Empty object or missing key");
        }
        if (pathArray[0].includes(".") || pathArray[0].includes("[")) {
            pathArray = pathArray[0].split(/[.[\]]+/);
        }
        return pathArray.reduce((previousValue, currentValue) => {
            if (placeHoldersObj) {
                const phKeys = Object.keys(placeHoldersObj);
                if (phKeys.includes(previousValue)) {
                    previousValue = placeHoldersObj[previousValue];
                } else if (phKeys.includes(currentValue)) {
                    currentValue = placeHoldersObj[currentValue];
                }
            }
            if (currentValue instanceof RegExp) {
                const regexpMatchingProperty = getMatchingPropertyName(previousValue, currentValue);
                return previousValue[regexpMatchingProperty];
            }
            if (currentValue in previousValue) {
                return previousValue[currentValue];
            }
            throw new Error("Broken property path");
        }, obj);
    } catch (error) {
        throw new Error(`Failed to get the property value: ${error}`);
    }
}

function getFromChromeStorage(key) {
    return new Promise((resolve, reject) => {
        if (chrome.runtime.lastError) {
            return reject(chrome.runtime.lastError);
        }
        chrome.storage.local.get(key, resolve);
    });
}

function saveToChromeStorage(obj) {
    return new Promise((resolve, reject) => {
        if (chrome.runtime.lastError) {
            return reject(chrome.runtime.lastError);
        }
        chrome.storage.local.set(obj, resolve);
    });
}

function isValidURL(url) {
    const regexp = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-/]))?/;
    return regexp.test(url);
}

function invalidMetaData(source) {
    displayOnThePopup("metaStatus", "Missing metadata");
    console.warn(`Missing metadata: ${source}`);
}

function showLoadingIndicator(state) {
    if (state) {
        document.getElementById("loader").style.display = "block";
    } else {
        document.getElementById("loader").style.display = "none";
    }
}

function verifyResponse(status, source) {
    if (status === 200) {
        return true;
    } else if (status === 404) {
        if (source === "ci") {
            hideFromThePopup("latestBuild", "latestBuildCheckbox");
            hideFromThePopup("latestFinishedBuild", "latestFinishedBuildCheckbox");
            displayOnThePopup("actionStatus", "Branch not found.");
            console.warn("404: CI fetchJSON");
        }
        if (source === "cd") {
            console.warn("404: CD fetchJSON");
        }
        return false;
    } else if (status === 401) {
        if (source === "ci") {
            document.getElementById("ciStatus").innerHTML = `<a href="${gl.ciLoginUrl}">Log in to CI</a>`;
            console.warn("401: CI fetchJSON");
        } else if (source === "cd") {
            document.getElementById("cdStatus").innerHTML = `<a href="${gl.cdLoginUrl}">Log in to CD</a>`;
            console.warn("401: CD fetchJSON");
        } else if (source === "its") {
            document.getElementById("itsStatus").innerHTML = `<a href="${gl.itsUrl[0]}${gl.itsUrl[2]}">Log in to ITS</a>`;
            console.warn("401: ITS fetchJSON");
        }
        return false;
    }
    return false;
}

function fetchJSON(file, source) {
    return new Promise((resolve, reject) => {
        if (isValidURL(file)) {
            showLoadingIndicator(true);
            fetch(`${file}?${new Date().getTime()}`, {
                method: "GET",
                headers: {
                    "Accept": "application/json",
                    "Cache-Control": "no-cache",
                },
            }).then(response => {
                if (verifyResponse(response.status, source)) {
                    showLoadingIndicator(false);
                    resolve(response.text());
                } else {
                    showLoadingIndicator(false);
                    invalidMetaData(source);
                    return reject(`Failed to fetch: ${source}`);
                }
            }).catch(error => {
                showLoadingIndicator(false);
                invalidMetaData(source);
                return reject(`Failed to fetch: ${source}, ${error}`);
            });
        } else {
            invalidMetaData(source);
            return reject(`Failed to fetch: ${source}`);
        }
    });
}

function iterateArgs(fn, argsArray) {
    if (typeof fn === "function" && Array.isArray(argsArray)) {
        const requestPromises = argsArray.map(file => fn(file));
        return Promise.allSettled(requestPromises).then(outcomes => {
            const result = outcomes.filter(outcome => outcome.status === "fulfilled");
            return result[0].value;
        }).catch(error => {
            throw new Error(`Error during function argument iteration: ${error}`);
        });
    }
    return Promise.reject(new Error("Invalid function parameters. Requires: fn, array"));
}

function isDeliveryNameMatchEnv(obj, path, env) {
    return !!getValue(obj, path).match(env);
}

function isDeliverySlotsMatchEnv(arr, path, env) {
    const envCorrectDelivery = arr.filter(endpoint => env.test(getValue(endpoint, path)));
    return !!envCorrectDelivery.length;
}

function isUrlRelated(arr, path, url) {
    arr = Array.isArray(arr) ? arr : [arr];
    const urlCorrectDelivery = arr.filter(endpoint => url.includes(getValue(endpoint, path)));
    return !!urlCorrectDelivery.length;
}

function isProductRelated(obj, path, product) {
    return getValue(obj, path).includes(product);
}

function extractVersionNumber(obj, path) {
    return getValue(obj, path).match(/\d+(\.\d+)+/g);
}

function isUiVerRelated(obj, path, uiVer) {
    return getValue(obj, path) === uiVer;
}

function browserVersion() {
    const label = "Browser:";
    const ver = /Chrome\/([0-9.]+)/.exec(navigator.userAgent)[1];
    displayOnThePopup("browser", `${label} ${ver}`, "browserCheckbox");
}

async function showViewportSize() {
    const label = "Viewport:";
    const size = await getFromChromeStorage(["sWidth", "sHeight"]);
    displayOnThePopup("viewportSize", `${label} ${size.sWidth} x ${size.sHeight}`, "viewportSizeCheckbox");
}

function currentUrl(url) {
    const label = "URL:";
    displayOnThePopup("url", `${label} ${url}`, "urlCheckbox");
}

function getEnvRegExp(environment) {
    if (environment) {
        if (/^pr.?d.*/.test(environment)) {
            return RegExp(/^pr.?d.*/);
        }
        return new RegExp(environment);
    }
    return null;
}

function getDynamicVarName(suffix, product, env) {
    if (suffix) {
        suffix = `_${suffix}`;
        // Removes the special characters from the product name
        product = product ? product.replace(/[^\w]/g, "").toLowerCase() : "";
        env = env ? `_${env}` : "";
        return `${product}${env}${suffix}`;
    }
    return null;
}

function uiVersion(file) {
    return fetchJSON(`${rt.shortenedUrl}${file}`, "uiVersion")
        .then(response => JSON.parse(response))
        .then(data => {
            const label = "UI version:";
            let ver = data[gl.productUiVerKey];
            if (ver) {
                if (ver.includes("-")) {
                    // Removes the unnecessary build number from the end
                    ver = ver.match(/.*(?=-)/)[0];
                }
                displayOnThePopup("uiVersion", `${label} ${ver}`, "uiVerCheckbox");
                return ver;
            }
            return null;
        }).catch(error => {
            throw new Error(`Failed to get the UI Version: ${error}`);
        });
}

function currentProduct(file) {
    return fetchJSON(`${rt.shortenedUrl}${file}`, "product")
        .then(response => JSON.parse(response))
        .then(data => {
            const label = "Product:";
            let product = data[gl.productNameKey];
            if (product) {
                if (product.includes("/")) {
                    // Keeps the characters after the first slash character
                    product = product.match(/(?<=\/).*/)[0];
                }
                displayOnThePopup("product", `${label} ${product}`, "productCheckbox");
                return product;
            }
            return null;
        }).catch(error => {
            throw new Error(`Failed to get the Product Name: ${error}`);
        });
}

function host(file) {
    return fetchJSON(`${rt.shortenedUrl}${file}`, "host")
        .then(response => {
            // Removes all the characters before the first curly brackets and after the last curly brackets
            const data = response.replace(/^[^{]+/g, "").replace(/[^}]*$.*/g, "");
            return JSON.parse(data);
        }).then(obj => {
            const label = "Host:";
            let environment;
            gl.productHostKeys.forEach(key => {
                if (obj[key]) {
                    environment = obj[key].toLowerCase();
                }
            });
            if (environment) {
                displayOnThePopup("host", `${label} ${environment}`, "hostCheckbox");
                return environment;
            }
            return null;
        }).catch(error => {
            throw new Error(`Failed to get the Host: ${error}`);
        });
}

function getCiLatestBuild(branch, displayed = true) {
    return fetchJSON(`${gl.ciLatestBuildUrl}${branch}`, "ci")
        .then(response => JSON.parse(response))
        .then(data => {
            const label = "Build in progress:";
            const txt = data[gl.ciBuildingKey].toString();
            const warning = `${label} <font color="red">${txt}</font>`;
            const noWarning = `${label} <font color="green">${txt}</font>`;
            if (displayed) {
                if (txt.includes("true")) {
                    displayOnThePopup("latestBuild", warning, "latestBuildCheckbox");
                } else {
                    displayOnThePopup("latestBuild", noWarning, "latestBuildCheckbox");
                }
            }
            return !!txt;
        }).catch(error => {
            console.error(`Failed to get the Latest Build Info: ${error}`);
        });
}

function getCiLatestFinishedBuild(branch, displayed = true) {
    fetchJSON(`${gl.ciLatestFinishedBuildUrl[0]}${branch}${gl.ciLatestFinishedBuildUrl[1]}`, "ci")
        .then(response => JSON.parse(response))
        .then(data => {
            const label = "Latest finished build:";
            const txt = data[gl.ciBuildStateKey].toString();
            const warning = `${label} <font color="red">${txt}</font>`;
            const noWarning = `${label} <font color="green">${txt}</font>`;
            if (displayed) {
                if (txt.includes("Successful")) {
                    displayOnThePopup("latestFinishedBuild", noWarning, "latestFinishedBuildCheckbox");
                } else {
                    displayOnThePopup("latestFinishedBuild", warning, "latestFinishedBuildCheckbox");
                }
            }
        }).catch(error => {
            console.error(`Failed the get the Latest Finished Build Info: ${error}`);
        });
}

function parseDeploymentStateCode(deploymentState) {
    switch (deploymentState) {
        case 0:
            return "<font color=\"green\">Successful</font>";
        case 1:
            return "<font color=\"red\">In progress</font>";
        case 2:
            return "<font color=\"red\">Error during deployment</font>";
        case 3:
            return "<font color=\"red\">Failed deployment</font>";
        case 4:
            return "<font color=\"red\">IDLE</font>";
        case 5:
            return "<font color=\"green\">Promoted</font>";
        default:
            return deploymentState;
    }
}

function getDeliveryType(trimmedUiVer) {
    if (trimmedUiVer) {
        const isFeatureType = /[a-zA-Z]/.test(trimmedUiVer.toString());
        if (isFeatureType) {
            return "feature";
        }
        return "release";
    }
    return null;
}

function getDelivery(envRegExp, product) {
    return fetchJSON(`${gl.cdDeliveriesUrl}#`, "cd")
        .then(response => JSON.parse(response))
        .then(data => {
            const deliveries = getValue(data, gl.cdDeliveryKeys[0]);
            const matchingDeliveries = deliveries.filter(delivery => isProductRelated(delivery, gl.cdDeliveryKeys[1], product));
            let envCorrectDelivery = matchingDeliveries.filter(delivery => isDeliveryNameMatchEnv(delivery, gl.cdDeliveryKeys[2], envRegExp));
            if (envCorrectDelivery.length === 1) {
                return getValue(envCorrectDelivery, gl.cdDeliveryKeys[2]);
            }
            if (matchingDeliveries.length > 1) {
                let urlCorrectDelivery = matchingDeliveries.filter(
                    delivery => isUrlRelated(delivery[gl.cdDeliveryKeys[3]], gl.cdDeliveryKeys[6], rt.shortenedUrl),
                );
                if (!urlCorrectDelivery.length) {
                    urlCorrectDelivery = matchingDeliveries.filter(
                        delivery => isUrlRelated(delivery[gl.cdDeliveryKeys[4]], gl.cdDeliveryKeys[6], rt.shortenedUrl),
                    );
                }
                if (urlCorrectDelivery.length === 1) {
                    return getValue(urlCorrectDelivery, gl.cdDeliveryKeys[2]);
                }
                envCorrectDelivery = matchingDeliveries.filter(
                    delivery => isDeliverySlotsMatchEnv(delivery[gl.cdDeliveryKeys[5]], gl.cdDeliveryKeys[7], envRegExp),
                );
                if (envCorrectDelivery.length > 1) {
                    envCorrectDelivery = envCorrectDelivery.filter(
                        delivery => isUrlRelated(delivery, gl.cdDeliveryKeys[2], rt.shortenedUrl),
                    );
                }
                return getValue(envCorrectDelivery, gl.cdDeliveryKeys[2]);
            }
        }).catch(error => {
            throw new Error(`Failed to get the Delivery: ${error}`);
        });
}

function getBuildNr(endpoints, placeHolders, trimmedUiVer) {
    let deployedBuildNr;
    let latestBuildNr;
    const buildNumberLabel = "Build Nr.:";
    endpoints.forEach(endpoint => {
        if (isUiVerRelated(endpoint, gl.cdBuildKeys[1], trimmedUiVer)) {
            deployedBuildNr = getValue(endpoint, gl.cdBuildKeys[2], placeHolders);
            latestBuildNr = getValue(endpoint, gl.cdBuildKeys[4], placeHolders);
        } else {
            deployedBuildNr = getValue(endpoints[0], gl.cdBuildKeys[2], placeHolders);
            latestBuildNr = getValue(endpoints[0], gl.cdBuildKeys[4], placeHolders);
        }
    });
    if (deployedBuildNr) {
        if (Array.isArray(latestBuildNr)) {
            latestBuildNr = latestBuildNr.slice(0).sort((a, b) => b - a)[0];
        }
        const deployedAndLatestBuildNr = `${deployedBuildNr} (${latestBuildNr ? latestBuildNr : ""})`;
        displayOnThePopup("buildNumber", `${buildNumberLabel} ${deployedAndLatestBuildNr}`, "buildNumberCheckbox");
    } else {
        throw new Error("Failed to get the build number.");
    }
}

function getDeploymentState(endpoints, placeHolders, trimmedUiVer) {
    let deploymentState;
    const deploymentStateLabel = "Deployment state:";
    endpoints.forEach(endpoint => {
        if (isUiVerRelated(endpoint, gl.cdBuildKeys[1], trimmedUiVer)) {
            deploymentState = getValue(endpoint, gl.cdBuildKeys[3], placeHolders);
        } else {
            deploymentState = getValue(endpoints[0], gl.cdBuildKeys[3], placeHolders);
        }
    });

    if (Number.isInteger(+deploymentState)) {
        const state = parseDeploymentStateCode(deploymentState);
        displayOnThePopup("deploymentState", `${deploymentStateLabel} ${state}`, "deploymentStateCheckbox");
    } else {
        throw new Error("Failed to get the deployment state.");
    }
}

async function getBuildNrAndDeploymentState(environment, product, trimmedUiVer) {
    const deliveryType = getDeliveryType(trimmedUiVer);
    const envRegExp = getEnvRegExp(environment);
    const cdDelivery = await getDelivery(envRegExp, product);
    if (deliveryType && envRegExp && cdDelivery) {
        const url = `${gl.cdSearchUrl[0]}"${cdDelivery}"${gl.cdSearchUrl[1]}"${deliveryType}"${gl.cdSearchUrl[2]}"${trimmedUiVer}"#`;
        fetchJSON(url, "cd")
            .then(response => JSON.parse(response))
            .then(data => {
                const placeHolders = {};
                const endpoints = getValue(data, gl.cdBuildKeys[0]);
                placeHolders.env = envRegExp;
                getDeploymentState(endpoints, placeHolders, trimmedUiVer);
                getBuildNr(endpoints, placeHolders, trimmedUiVer);
            }).catch(error => {
                console.error(`Failed to get the Build Nr/Deployment State Info: ${error}`);
            });
    } else {
        console.warn("Failed to get the Build Nr/Deployment State Info, missing: deliveryType, envRegExp, cdDelivery");
    }
}

function getAutoCompleteSvcVersion(environment, product) {
    const label = "Auto Complete:";
    const fileToFetch = getDynamicVarName("autoCompleteSvcVerUrl", product, environment);
    if (gl[fileToFetch]) {
        fetchJSON(gl[fileToFetch], "autoCompleteSvcVer")
            .then(response => JSON.parse(response))
            .then(data => {
                const ver = getValue(data, gl.autoCompleteVerKey);
                displayOnThePopup("autoCompleteSvcVer", `${label} ${ver} ${gl.auxDataUrl}`, "autoCompleteSvcVerCheckbox");
            }).catch(error => {
                console.error(`Failed to get the Auto-Complete Version: ${error}`);
            });
    } else {
        console.warn("Failed to get the Auto-Complete Version: source file is undefined.");
    }
}

function getPlatformVersion(environment, product) {
    const label = "Platform:";
    const fileToFetch = getDynamicVarName("platformVerUrl", product, environment);
    if (gl[fileToFetch]) {
        fetchJSON(gl[fileToFetch], "platformVer")
            .then(response => JSON.parse(response))
            .then(data => {
                const ver = getValue(data, gl.platformVerKey);
                displayOnThePopup("platformVer", `${label} ${ver}`, "platformVerCheckbox");
            }).catch(error => {
                console.error(`Failed to get the Platform Version: ${error}`);
            });
    } else {
        console.warn("Failed to get the Platform Version: source file is undefined.");
    }
}

function getSearchSvcVersion(environment, product) {
    const label = "Search Service:";
    const fileToFetch = getDynamicVarName("srchVerUrl", product, environment);
    if (gl[fileToFetch]) {
        fetchJSON(gl[fileToFetch], "srchVer")
            .then(response => JSON.parse(response))
            .then(data => {
                const path = getValue(data, gl.srchVerKey);
                displayOnThePopup("srchVer", `${label} ${path}`, "srchVerCheckbox");
            }).catch(error => {
                console.error(`Failed to get the Search Service Version: ${error}`);
            });
    } else {
        console.warn("Failed to get the Search Service Version: source file is undefined.");
    }
}

function getEmailSvcTemplateVersion(environment, product) {
    const label = "Email Template:";
    const fileToFetch = getDynamicVarName("emailTempUrl", product, environment);
    if (gl[fileToFetch]) {
        fetchJSON(gl[fileToFetch], "emailSvcTempVer")
            .then(response => JSON.parse(response))
            .then(data => {
                const versionsByProduct = getValue(data, gl.emailTempKeys[0]);
                versionsByProduct.forEach(element => {
                    if (isProductRelated(element, gl.emailTempKeys[1], product)) {
                        const versionNumber = extractVersionNumber(element, [gl.emailTempKeys[1]]);
                        displayOnThePopup("emailSvcTempVer", `${label} ${versionNumber[0]}`, "emailSvcTempVerCheckbox");
                    }
                });
            }).catch(error => {
                console.error(`Failed to get the Email Template Version: ${error}`);
            });
    } else {
        console.warn("Failed to get the Email Template Version: source file is undefined.");
    }
}

function getBranchKey(product, trimmedUiVer) {
    const regexMatchIssueId = new RegExp(/(?<=-)[A-Za-z]{2,}-\d+/);
    const regexMatchLetters = new RegExp(/[a-zA-Z].*/);
    const productPlanKey = getDynamicVarName("masterPlanKey", product);
    if (gl[productPlanKey]) {
        if (regexMatchIssueId.test(trimmedUiVer)) {
            trimmedUiVer = trimmedUiVer.match(regexMatchIssueId)[0];
        } else if (regexMatchLetters.test(trimmedUiVer)) {
            trimmedUiVer = trimmedUiVer.match(regexMatchLetters)[0];
        }
        fetchJSON(`${gl.ciSearchUrl[0]}${trimmedUiVer}${gl.ciSearchUrl[1]}${gl[productPlanKey]}#`, "ci")
            .then(response => JSON.parse(response))
            .then(data => {
                branchForm.value = getValue(data, gl.ciPlanKey);
            }).catch(error => {
                console.error(`Failed to get the branchKey: ${error}`);
            });
    } else {
        console.warn("Failed to get the branchKey: productPlanKey is undefined.");
    }
}

// Saves form data into chrome.storage
async function saveForm(callback) {
    const obj = {};
    const branch = branchForm.value;
    if (branch) {
        if (await getCiLatestBuild(branch, false)) {
            obj[rt.urlFingerprint] = branch;
            chrome.storage.local.set(obj, () => {
                if (!chrome.runtime.lastError) {
                    const status = document.getElementById("actionStatus");
                    status.textContent = "The branch is set.";
                    setTimeout(() => {
                        status.textContent = "";
                    }, 1000);
                }
            });
            rt.linkToCI = gl.ciBrowseUrl + branch;
            displayOnThePopup("ci");
            callback();
        }
    }
}

// Restores form data from chrome.storage
function restoreForm(product, trimmedUiVer) {
    chrome.storage.local.get([rt.urlFingerprint], items => {
        if (items[rt.urlFingerprint]) {
            branchForm.value = items[rt.urlFingerprint];
            rt.linkToCI = gl.ciBrowseUrl + items[rt.urlFingerprint];
            getCiLatestBuild(items[rt.urlFingerprint]);
            getCiLatestFinishedBuild(items[rt.urlFingerprint]);
            displayOnThePopup("ci");
        } else {
            getBranchKey(product, trimmedUiVer);
            console.log("No saved branch for this page");
        }
    });
}

// Removes form data from chrome.storage
function removeForm() {
    const branch = branchForm.value;
    if (branch && branch !== "") {
        chrome.storage.local.remove([rt.urlFingerprint], () => {
            if (!chrome.runtime.lastError) {
                const status = document.getElementById("actionStatus");
                status.textContent = "Branch removed.";
                setTimeout(() => {
                    status.textContent = "";
                }, 1000);
            }
            hideFromThePopup("ci");
            console.log("The branch is removed");
        });
    } else {
        console.log("The form is empty already");
    }
    hideFromThePopup("branch");
    hideFromThePopup("ciStatus");
    hideFromThePopup("latestBuild", "latestBuildCheckbox");
    hideFromThePopup("latestFinishedBuild", "latestFinishedBuildCheckbox");
}

// Opens a its ticket
function openTicket() {
    let id = ticketForm.value;
    if (id && gl.itsUrl && gl.itsDefaultProject) {
        if (id.indexOf("-") > -1) {
            const idWithProject = id.split("-");
            const project = idWithProject[0];
            id = +idWithProject[1];
            if (Number.isInteger(Number(id)) && Number(id) > 0) {
                return `${gl.itsUrl[0]}${gl.itsUrl[1]}${project}-${id}`;
            }
            displayOnThePopup("actionStatus", "Invalid ticket ID");
            return null;
        } else if (Number.isInteger(Number(id)) && Number(id) > 0) {
            return `${gl.itsUrl[0]}${gl.itsUrl[1]}${gl.itsDefaultProject}-${id}`;
        }
        displayOnThePopup("actionStatus", "Invalid ticket ID");
        return null;
    }
    displayOnThePopup("actionStatus", "Invalid ticket ID");
    return null;
}

function getParentIssueId(url, trimmedUiVer) {
    const ticketIdRegExp = new RegExp(/(?<=-)[A-Za-z]{2,}-\d+/);
    if (ticketIdRegExp.test(trimmedUiVer)) {
        trimmedUiVer = trimmedUiVer.match(ticketIdRegExp)[0];
        fetchJSON(`${url}${trimmedUiVer}#`, "its")
            .then(response => JSON.parse(response))
            .then(data => {
                rt.parentIssueId = data.id;
            }).catch(error => {
                console.error(`Failed to get the parentIssueId: ${error}`);
            });
    } else {
        console.warn("Failed to get the parentIssueId: no ticket ID in the UI version string.");
    }
}

function getCheckmarkedData() {
    return Array.from(document.querySelectorAll(".data > input:checked"))
        .map(check => check.nextElementSibling.innerText)
        .filter(check => !!check);
}

async function thereIsSavedEndpointInfo() {
    const data = await getFromChromeStorage(["endpointInfos"]);
    if (data.endpointInfos) {
        displayOnThePopup("apply");
    }
}

function createTicket(bugForm, defectForm) {
    const issueType = document.getElementById("issueType").value;
    const parrentIssueId = rt.parentIssueId;
    const selectedEndpointInfos = getCheckmarkedData();
    saveToChromeStorage({
        endpointInfos: selectedEndpointInfos,
        ticketType: issueType,
    });
    if (issueType === "bug") {
        if (parrentIssueId) {
            chrome.tabs.create({
                url: `${bugForm}${parrentIssueId}`,
            });
        } else {
            displayOnThePopup("actionStatus", "Missing parrentIssueId");
        }
    } else if (issueType === "defect") {
        chrome.tabs.create({
            url: defectForm,
        });
    }
}

function main(globalsAreAvailable = false) {
    chrome.tabs.query({
        currentWindow: true,
        active: true,
    }, tabs => {
        const tabUrl = tabs[0].url;
        rt.shortenedUrl = tabUrl.replace(/^((\w+:)?\/\/[^/]+\/?).*$/, "$1");
        rt.urlFingerprint = `usr${rt.shortenedUrl.replace(/\W/g, "")}`;
        browserVersion();
        showViewportSize();
        currentUrl(rt.shortenedUrl);
        thereIsSavedEndpointInfo();
        if (globalsAreAvailable) {
            Promise.allSettled([
                iterateArgs(host, gl.hostFiles),
                currentProduct(gl.productNameFile),
                iterateArgs(uiVersion, gl.uiVerFiles),
            ]).then(results => {
                restoreForm(results[1].value, results[2].value);
                getBuildNrAndDeploymentState(results[0].value, results[1].value, results[2].value);
                getPlatformVersion(results[0].value, results[1].value);
                getSearchSvcVersion(results[0].value, results[1].value);
                getAutoCompleteSvcVersion(results[0].value, results[1].value);
                getEmailSvcTemplateVersion(results[0].value, results[1].value);
                getParentIssueId(gl.itsGetIssueIdUrl, results[2].value);
            }).catch(error => {
                console.error(error);
            }).finally(() => {
                displayOnThePopup("create");
                console.log("main: executed");
            });
        } else {
            hideFromThePopup("save");
            hideFromThePopup("remove");
        }
    });
}

function sendMessage(request) {
    return new Promise((resolve, reject) => {
        chrome.runtime.sendMessage(request, response => {
            if (response) {
                resolve(response);
            } else {
                return reject(response);
            }
        });
    });
}

// Executed at every opening
document.addEventListener("DOMContentLoaded", () => {
    sendMessage({
        command: "initVars",
    }).then(globals => {
        Object.assign(gl, globals);
        Object.freeze(gl);
        main(true);
    }).catch(error => {
        console.warn(`Globals are not available: ${error}`);
        main(false);
    });
});

// Sets all links clickable
window.addEventListener("click", element => {
    if (element.target.href) {
        chrome.tabs.create({
            url: element.target.href,
        });
    }
});

// The Enter key triggers the Set button and the Del key deletes the content of the branch form
branchForm.addEventListener("keyup", event => {
    if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("save").click();
    } else if (event.key === "Delete") {
        event.preventDefault();
        hideFromThePopup("branch");
    }
});

// The Enter key triggers the Open button and the Del key deletes the content of the ticket form
ticketForm.addEventListener("keyup", event => {
    displayOnThePopup("open");
    if (event.key === "Enter") {
        event.preventDefault();
        document.getElementById("open").click();
    } else if (event.key === "Delete") {
        event.preventDefault();
        hideFromThePopup("ticket");
    }
});

// Saves the branch name
document.getElementById("save").onclick = () => {
    saveForm(() => {
        restoreForm();
    });
};

// Removes the configured branch name from the endpoint
document.getElementById("remove").onclick = () => {
    removeForm();
};

// Link to the CI page of the branch
document.getElementById("ci").onclick = () => {
    chrome.tabs.create({
        url: rt.linkToCI,
    });
};

// Option page
document.getElementById("settings").onclick = () => {
    chrome.tabs.create({
        url: "options.html",
    });
};

// Link to a given its ticket
document.getElementById("open").onclick = () => {
    const ticketUrl = openTicket();
    if (ticketUrl) {
        chrome.tabs.create({
            url: ticketUrl,
        });
    }
};

// Link to a Bug-report/Defect its ticket creation form
document.getElementById("create").onclick = () => {
    createTicket(gl.itsCreateBugReportUrl, gl.itsCreateDefectUrl);
};

// Sends 'fill its ticket form' command to the service worker
document.getElementById("apply").onclick = () => {
    sendMessage({
        command: "fillForm",
    }).then(response => {
        console.log(response.result);
    }).catch(error => {
        console.error(`No response for the ticket fill command: ${error}`);
    });
};
