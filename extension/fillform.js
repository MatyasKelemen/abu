"use strict";

function getFromChromeStorage(key) {
    return new Promise((resolve, reject) => {
        if (chrome.runtime.lastError) {
            return reject(chrome.runtime.lastError);
        }
        chrome.storage.local.get(key, resolve);
    });
}

function getAdditionalInfos(allInfo) {
    const keysOfTheExtraInfos = [
        "Build Nr.",
        "Deployment state",
        "Platform",
        "Search Service",
        "Auto Complete",
        "Email Template",
        "Latest finished build",
        "Build in progress",
    ];
    const extraInfos = Object.keys(allInfo).map(key => {
        if (keysOfTheExtraInfos.includes(key)) {
            return `\n${key}: ${allInfo[key]}`;
        }
        return "";
    });
    return extraInfos.filter(element => !!element);
}

function removeIndent(str) {
    return str ? str.replace(/\x20{2,}/g, "") : "";
}

function bugTicketTemplate(infos) {
    const temp = `*Background:*
    Affected layout(s): L (${infos.Viewport})
    Affected platform(s): Chrome ${infos.Browser.split(".")[0]},
    Affected environment(s): ${infos.Host}
    Known affected UI version(s): ${infos["UI version"]}
    Endpoint: ${infos.URL} ${getAdditionalInfos(infos)}
    
    *Steps to reproduce:*
    # Open the endpoint and log in with an entitled user
        
    *Actual result:*
        
    *Expected result:*\n`;
    return infos ? removeIndent(temp) : "";
}

function defectTicketTemplate(infos) {
    const temp = `*Background:*
    Affected layout(s): L (${infos.Viewport})
    Affected platform(s): Chrome ${infos.Browser.split(".")[0]},
    Affected environment(s): ${infos.Host}
    Known affected UI version(s): ${infos["UI version"]} ${getAdditionalInfos(infos)}
    
    *Steps to reproduce:*
    # Open the application and log in with an entitled user
    
    *Actual result:*
    
    *Expected result:*\n`;
    return infos ? removeIndent(temp) : "";
}

(async function fillTicketForm() {
    const endpointInfos = getFromChromeStorage(["endpointInfos"]);
    const editDescOnly = getFromChromeStorage(["editDescOnly"]);
    const ticketType = getFromChromeStorage(["ticketType"]);
    const storage = await Promise.allSettled([endpointInfos, editDescOnly, ticketType]);
    const infos = Object.fromEntries(storage[0].value.endpointInfos.map(item => item.split(": ")));
    const inputFieldTemplate = storage[2].value.ticketType === "bug" ? bugTicketTemplate(infos) : defectTicketTemplate(infos);
    document.getElementById("description").value = inputFieldTemplate;
    if (storage[1].value.editDescOnly === "Off") {
        document.getElementById("summary").value = `[${infos.Product}]`;
        document.getElementById("labels-textarea").value = "draft";
        if (storage[2].value.ticketType === "defect") {
            document.getElementById("environment").value = infos.URL;
        }
    }
}());
