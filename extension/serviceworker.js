"use strict";

// Returns the global variables within a Promise
function initVariables() {
    return new Promise((resolve, reject) => {
        chrome.storage.local.get(["globals"], items => {
            if (chrome.runtime.lastError) {
                return reject(chrome.runtime.lastError);
            }
            resolve(items.globals);
        });
    });
}

function showOptionPage() {
    const url = chrome.runtime.getURL("options.html");
    chrome.tabs.create({
        url,
    });
}

function showManual() {
    const url = chrome.runtime.getURL("man.html");
    chrome.tabs.create({
        url,
    });
}

function executeForegroundScriptViewPort() {
    chrome.tabs.query({
        active: true,
        currentWindow: true,
    }, tabs => {
        if (tabs && tabs.length > 0) {
            chrome.scripting.executeScript({
                target: {
                    tabId: tabs[0].id,
                },
                files: ["./viewport.js"],
            }).then(() => {
                console.log("Saving viewport size...");
            }).catch(error => {
                console.error(`Failed viewport size saving: ${error}`);
            });
        }
    });
}

function executeForegroundScriptFillForm(callback) {
    chrome.tabs.query({
        active: true,
        currentWindow: true,
    }, tabs => {
        if (tabs && tabs.length > 0) {
            chrome.scripting.executeScript({
                target: {
                    tabId: tabs[0].id,
                },
                files: ["./fillform.js"],
            }).then(() => {
                console.log("Filling ITS form...");
                callback();
            }).catch(error => {
                console.error(`Failed fill form action: ${error}`);
            });
        }
    });
}

chrome.runtime.onInstalled.addListener(({reason}) => {
    if (reason === chrome.runtime.OnInstalledReason.INSTALL) {
        showOptionPage();
    } else if (reason === chrome.runtime.OnInstalledReason.UPDATE) {
        showManual();
    }
});

chrome.windows.onFocusChanged.addListener(() => {
    executeForegroundScriptViewPort();
});

chrome.windows.onBoundsChanged.addListener(() => {
    executeForegroundScriptViewPort();
});

chrome.runtime.onMessage.addListener(
    (request, sender, sendResponse) => {
        if (request.command === "initVars") {
            initVariables().then(globals => {
                sendResponse(globals);
            }).catch(error => {
                console.error(`Failed to initialize the globals: ${error}`);
            });
            return true;
        } else if (request.command === "fillForm") {
            executeForegroundScriptFillForm(() => sendResponse({
                result: `Form fill action executed. Sender: ${sender?.id}`,
            }));
            return true;
        }
    },
);
