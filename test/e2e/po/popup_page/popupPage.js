"use strict";

const Page = require("../base_page/page");
const selectors = require("./selectors.js");
const { loadSelector } = require("../../utils/helpers");

class PopupPage extends Page {
    constructor(page) {
        super(page);

        this.body = page.$(loadSelector(selectors));
        this.branchForm = page.$(loadSelector(selectors, "branchForm"));
        this.branchFormInput = page.$(loadSelector(selectors, "branchFormInput"));
        this.branchSaveButton = page.$(loadSelector(selectors, "branchSaveButton"));
        this.branchRemoveButton = page.$(loadSelector(selectors, "branchRemoveButton"));
        this.goToCiButton = page.$(loadSelector(selectors, "goToCiButton"));
        this.ticketForm = page.$(loadSelector(selectors, "ticketForm"));
        this.ticketFormInput = page.$(loadSelector(selectors, "ticketFormInput"));
        this.openTicketButton = page.$(loadSelector(selectors, "openTicketButton"));
        this.issueTypeSelect = page.$(loadSelector(selectors, "issueTypeSelect"));
        this.bugIssueType = page.$(loadSelector(selectors, "bugIssueType"));
        this.defectIssueType = page.$(loadSelector(selectors, "defectIssueType"));
        this.createTicketButton = page.$(loadSelector(selectors, "createTicketButton"));
        this.fillTicketButton = page.$(loadSelector(selectors, "fillTicketButton"));
        this.endpointInfos = page.$(loadSelector(selectors, "endpointInfos"));
        this.checkboxes = page.$(loadSelector(selectors, "checkboxes"));
        this.infoParagraphs = page.$(loadSelector(selectors, "infoParagraphs"));
        this.statusbar = page.$(loadSelector(selectors, "statusbar"));
        this.leftStatusbar = page.$(loadSelector(selectors, "leftStatusbar"));
        this.ciStatus = page.$(loadSelector(selectors, "ciStatus"));
        this.itsStatus = page.$(loadSelector(selectors, "itsStatus"));
        this.cdStatus = page.$(loadSelector(selectors, "cdStatus"));
        this.metaStatus = page.$(loadSelector(selectors, "metaStatus"));
        this.rightStatusbar = page.$(loadSelector(selectors, "rightStatusbar"));
        this.actionStatus = page.$(loadSelector(selectors, "actionStatus"));
        this.settingsImg = page.$(loadSelector(selectors, "settingsImg"));
        this.loadingIndicator = page.$(loadSelector(selectors, "loadingIndicator"));
    }

    /**
     * desc.
     *
     * @returns {Promise.<boolean>}
     */
    async isSettled() {
        try {
            await this.page.waitForSelector(loadSelector(selectors, "loadingIndicator"), {
                hidden: true,
                timeout: 3000,
            });
        } catch (error) {
            return false;
        }
        return true;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    fillForm() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getBranchKey() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getTicketId() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getIssueType() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getStatusMessage() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getCheckboxState() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getSelectedInfos() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    selectInfo() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    deselectInfo() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    openSettings() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    openCi() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    openTicket() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    setBranch() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    deleteBranch() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    setIssueType() {
        return false;
    }
}

module.exports = PopupPage;
