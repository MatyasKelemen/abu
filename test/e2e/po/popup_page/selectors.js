module.exports = {
    selector: "#popupBody",
    children: {
        branchForm: {
            selector: ".branchForm",
            children: {
                branchFormInput: "#branch",
                branchSaveButton: "#save",
                branchRemoveButton: "#remove",
                goToCiButton: "#ci",
            },
        },
        ticketForm: {
            selector: ".ticketForm",
            children: {
                ticketFormInput: "#ticket",
                openTicketButton: "#open",
                issueTypeSelect: {
                    selector: "#issueType",
                    children: {
                        bugIssueType: "#bug",
                        defectIssueType: "#defect",
                    },
                },
                createTicketButton: "#create",
                fillTicketButton: "#apply",
            },
        },
        endpointInfos: {
            selector: ".data",
            children: {
                checkboxes: "[type=checkbox]",
                infoParagraphs: "p",
            },
        },
        statusbar: {
            selector: ".status",
            children: {
                leftStatusbar: {
                    selector: "#leftStatusContainer",
                    children: {
                        ciStatus: "#ciStatus",
                        itsStatus: "#itsStatus",
                        cdStatus: "#cdStatus",
                        metaStatus: "#metaStatus",
                    },
                },
                rightStatusbar: {
                    selector: "#rightStatusContainer",
                    children: {
                        actionStatus: "#actionStatus",
                    },
                },
            },
        },
        settingsImg: "#settings",
        loadingIndicator: "#loader",
    },
};
