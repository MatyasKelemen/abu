"use strict";

const Page = require("../base_page/page");
const selectors = require("./selectors.js");
const { loadSelector } = require("../../utils/helpers");

class OptionsPage extends Page {
    constructor(page) {
        super(page);

        this.body = page.$(loadSelector(selectors));
        this.storageClearButton = page.$(loadSelector(selectors, "storageClearButton"));
        this.branchClearButton = page.$(loadSelector(selectors, "branchClearButton"));
        this.hotkeyChangeButton = page.$(loadSelector(selectors, "hotkeyChangeButton"));
        this.fileInput = page.$(loadSelector(selectors, "fileInput"));
        this.statusDiv = page.$(loadSelector(selectors, "statusDiv"));
        this.userManualImg = page.$(loadSelector(selectors, "userManualImg"));
        this.separatorHr = page.$(loadSelector(selectors, "separatorHr"));
        this.globalsDiv = page.$(loadSelector(selectors, "globalsDiv"));
        this.globalsLabel = page.$(loadSelector(selectors, "globalsLabel"));
        this.globals = page.$(loadSelector(selectors, "globals"));
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    clearStorage() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    clearBranch() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    changeShortcut() {
        return false;
    }

    /**
     * elementHandle.uploadFile
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    importConfig() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    openManual() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getSettingsStatus() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getUrlOverview() {
        return false;
    }
}

module.exports = OptionsPage;
