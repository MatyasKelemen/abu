module.exports = {
    selector: "#optionBody",
    children: {
        storageClearButton: "#clear",
        branchClearButton: "#clearBranches",
        hotkeyChangeButton: "#hotkey",
        fileInput: "#fileInput",
        statusDiv: "#settingsStatus",
        userManualImg: "#userManual",
        separatorHr: "#line",
        globalsDiv: {
            selector: "#urlOverview",
            children: {
                globalsLabel: ".variableName",
                globals: ".variableValue",
            },
        },
    },
};
