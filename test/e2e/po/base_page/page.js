"use strict";

class Page {
    constructor(page) {
        this.page = page;
    }

    /**
     * Returns the visibility of a given element
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    async isVisible(element) {
        element = element ? await element : await this.body;
        if (element) {
            return element.isIntersectingViewport();
        }
        throw new Error(`Element not visible: ${element}`);
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getStatus() {
        return false;
    }

    /**
     * desc.
     *
     * @param {string}
     * @returns {Promise.<boolean>}
     */
    getElementState() {
        return false;
    }
}

module.exports = Page;
