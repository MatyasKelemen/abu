"use strict";

const { installExt } = require("./utils/helpers");
const OptionsPage = require("./po/options_page/optionsPage");
const expect = require("chai").expect;

describe("Extension Installation Test", () => {
    let browser; let optionsPageTab; let optionsPage;
    before(async () => {
        const context = await installExt();
        browser = context.browser;
        optionsPageTab = context.optionsTab;
        optionsPage = new OptionsPage(optionsPageTab);
    });

    after(async () => {
        await browser.close();
    });

    describe("Install", () => {
        it("should open the Options Page automatically", async () => {
            const page = await optionsPage.isVisible();
            expect(page).to.be.true;
        });
    });
});
