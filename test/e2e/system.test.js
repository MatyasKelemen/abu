"use strict";

const { installExt, openExt } = require("./utils/helpers");
const PopupPage = require("./po/popup_page/popupPage");
const expect = require("chai").expect;

describe("Extension System Test", () => {
    let browser; let popupTab; let popup;
    before(async () => {
        const context = await installExt();
        browser = context.browser;
    });

    after(async () => {
        await browser.close();
    });

    beforeEach(async () => {
        const context = await openExt(browser);
        popupTab = context.popupTab;
        popup = new PopupPage(popupTab);
    });

    afterEach(async () => {
        await popupTab.close();
    });

    describe("Extension", () => {
        it("should display the popup", async () => {
            const page = await popup.isVisible();
            expect(page).to.be.true;
        });
    });
});
