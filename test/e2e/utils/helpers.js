"use strict";

const puppeteer = require("puppeteer");
const path = require("path");
const TIMEOUT = 5000;

async function installExt() {
    const extensionPath = path.resolve("extension");
    const browser = await puppeteer.launch({
        headless: false,
        args: [
            `--disable-extensions-except=${extensionPath}`,
            `--load-extension=${extensionPath}`,
        ],
    });

    const [firstTab] = await browser.pages();
    await firstTab.goto("http://localhost:3000/", {
        waitUntil: "load",
    });
    const [, optionsTab] = await browser.pages();
    await firstTab.close();
    optionsTab.setDefaultTimeout(TIMEOUT);

    return {
        browser,
        optionsTab,
    };
}

async function openExt(browser) {
    const targets = await browser.targets();
    const extensionTarget = targets.find(target => target.type() === "service_worker");
    const partialExtensionUrl = extensionTarget._targetInfo.url || "";
    const [,, extensionId] = partialExtensionUrl.split("/");

    const popupTab = await browser.newPage();
    const popupUrl = `chrome-extension://${extensionId}/popup.html`;
    await popupTab.goto(popupUrl, {
        waitUntil: "load",
    });
    popupTab.setDefaultTimeout(TIMEOUT);

    return {
        popupUrl,
        popupTab,
    };
}

function findSelectorByName(selectors, name, acc = "") {
    if (selectors && typeof selectors === "object") {
        if (!name) {
            return `${acc} ${selectors.selector}`;
        }
        if (selectors.children) {
            return findSelectorByName(selectors.children, name, `${acc} ${selectors.selector}`);
        }
        const selectorKeys = Object.keys(selectors);
        for (const key of selectorKeys) {
            if (typeof selectors[key] === "object") {
                const foundSelector = findSelectorByName(selectors[key], key === name ? "" : name, acc);
                if (foundSelector) {
                    return foundSelector;
                }
            } else if (key === name) {
                return `${acc} ${selectors[key]}`;
            }
        }
    }
    return null;
}

function loadSelector(selectors, name) {
    const foundSelector = findSelectorByName(selectors, name);
    if (!foundSelector) {
        throw new Error(`Selector not found: ${name}`);
    }
    return foundSelector.trim();
}

module.exports = {
    installExt,
    openExt,
    loadSelector,
};
